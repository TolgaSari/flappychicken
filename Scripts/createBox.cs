using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createBox : MonoBehaviour
{
    public float createTime;
    private float createTimer = 100000f;
    public float offset = 0.5f;
    Vector2 position;
    GameObject temp;


    public GameObject box = null;

    int createCase;
    // Start is called before the first frame update
    void Start()
    {
        createTime = createTimer;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (createTime < 0f)
        {
            createTime = createTimer;

            float K = Random.Range(-offset, offset);

            position = new Vector2(transform.position.x,transform.position.y+K);
            Instantiate(box, position, transform.rotation);

        }
        else
        {
            createTime -= Time.fixedDeltaTime;
        }
    }
    public void setTimer(float time)
    {
        createTime = time;
        createTimer = time;
    }
}
