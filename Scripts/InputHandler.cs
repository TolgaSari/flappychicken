﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{

    [SerializeField]
    private KeyCode jumpKey = KeyCode.Space;

    public MovementHandler mHandler;
    public TapToStart tToStart;

    bool isTouched = false;

    // Start is called before the first frame update
    void Start()
    {
        mHandler = gameObject.GetComponent<MovementHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if(mHandler.dedAnim == false)
        {
            if (Input.GetKeyDown(jumpKey))
            {
                mHandler.attemptJump = true;
            }

            //if (Input.GetTouch(0).tapCount==1)
            //{
            //mHandler.attemptJump = true;
            //}

            if (!isTouched && Input.touchCount > 0)
            {
                if(Input.GetTouch(0).phase == 0)
                {
                    tToStart.isTapped = true;
                    mHandler.attemptJump = true;
                    isTouched = true;
                }
                
            }

            else
            {
                isTouched = false;
            }
        }
        
    }
}
