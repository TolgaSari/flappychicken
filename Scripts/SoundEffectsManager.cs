﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundEffectsManager : MonoBehaviour
{
    public Sound[] sounds;
    public static SoundEffectsManager instance;
    public bool soundOn;

    // Start is called before the first frame update
    void Awake()
    {
        soundOn = true;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.playOnAwake = false;
        }
    }
    
    public void PlayByNumber(int number)
    {
        if (soundOn == true)
        { 
            if(number < sounds.Length)
            {
                sounds[number].source.volume = 0.25f;
                sounds[number].source.Play();
            }
        }
    }


}
