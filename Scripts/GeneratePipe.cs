﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePipe : MonoBehaviour
{
    public float doubleDistance = 5f;
    public float createTime;
    public float createTimer = 2f;
    public float downMax = -1.9f;
    public float downMin = -2.7f;
    public float upMax = 1.7f;
    public float upMin = 2f;
    public float createX = 3f;
    Vector2 position;
    GameObject temp;




    public GameObject downPipe = null;
    public GameObject upPipe = null;

    int createCase;
    // Start is called before the first frame update
    void Start()
    {
        createTime = createTimer;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (createTime < 0f)
        {
            createTime = createTimer;

            //createCase = Random.Range(0, 3);
            createCase = 2;

            switch(createCase)
            {
                case 0: // upPipe
                    position = new Vector2(createX, Random.Range(upMin, upMax));
                    Instantiate(upPipe, position,upPipe.transform.rotation);
                    break;

                case 1: // downPipe
                    position = new Vector2(createX, Random.Range(downMin, downMax));
                    Instantiate(downPipe, position, downPipe.transform.rotation);
                    break;

                case 2: // doublePipe
                    float K = Random.Range(downMin, downMax);
                    position = new Vector2(createX,K);
                    Instantiate(downPipe, position, downPipe.transform.rotation);
                    position = new Vector2(createX, K+doubleDistance);
                    temp=Instantiate(upPipe, position, upPipe.transform.rotation);
                    temp.GetComponent<PipeMovements>().reverseSpeed();

                    break;
            }
        }
        else
        {
            createTime -= Time.fixedDeltaTime;
        }
    }
}
