﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Parallax : MonoBehaviour
{
    private float length, startpos;
    public float camSpeed = 0.1f;
    public Vector2 cam;
    public float parallaxEffect;
    // Start is called before the first frame update
    void Start()
    {
        cam = new Vector2(0, 0);
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float temp = (cam.x * (1 - parallaxEffect));
        float dist = (cam.x * parallaxEffect);

        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);
        
        if(dist > startpos + 2*length)
        {
            cam.x = startpos;
        }
        else if(dist < startpos - 2*length)
        {
            cam.x = startpos;
        }
        cam.x += Time.fixedDeltaTime * camSpeed;
    }
}
