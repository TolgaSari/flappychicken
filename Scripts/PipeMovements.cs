﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMovements : MonoBehaviour
{
    float timer = 10f;
    float speed = 0.5f;
    public void reverseSpeed()
    {
        speed = -speed;
    }
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, timer);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(Vector3.left * speed*Time.fixedDeltaTime);
    }
}
