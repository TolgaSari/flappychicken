using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;

public class MovementHandler : MonoBehaviour
{    
    public bool attemptJump = false;
    public bool youDead = false;
    public bool dedAnim = false;

    [SerializeField]
    private float jumpModifier = 4f;
    private Vector2 addForce;
    private Vector2 startPosition;
    private float jumpCD = 0f;
    private float timeUntilJump = 0f;
    private float DedDelay_ = 1f;

    private float dedSpeed;
    private float dedRotation;
    private float dedRotOffset = 90f;
    private float dedSpeedOffset = 0.3f;

    public Rigidbody2D rb2D = null;
    //public Collider2D collider;
    public int ballPoints = 10;

    public Animator animator = null;

    public GameObject synthAudioManager = null;
    private SynthAudioManager synthAManager;
    public TapToStart tapHandler;

    public GameObject sfxManager = null;
    public SoundEffectsManager SfxManager;

    public Text score;
    public Text scoreReload;
    private float seconds;
    //public float gravityScalee=0f;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.GetComponent<Rigidbody2D>())
        {
            rb2D = gameObject.GetComponent<Rigidbody2D>();
        }

        if (gameObject.GetComponent<Animator>())
        {
            animator = gameObject.GetComponent<Animator>();
        }

        //if (gameObject.GetComponent<Collider2D>())
        //{
        //    collider = gameObject.GetComponent<Collider2D>();
        //}


        synthAudioManager = GameObject.Find("SynthAudioManager");
        synthAManager = synthAudioManager.GetComponent<SynthAudioManager>();

        sfxManager = GameObject.Find("SoundEffectsManager");
        SfxManager = sfxManager.GetComponent<SoundEffectsManager>();

        dedAnim = false;

        startPosition = transform.position;

        // rb2D.gravityScale = 1.44f;
        rb2D.gravityScale = tapHandler.gravityScalee;
        Time.timeScale = 1f;

        seconds = 0;

        dedSpeed += Random.Range(-dedSpeedOffset, dedSpeedOffset);
        dedRotation += Random.Range(-dedRotOffset, dedRotOffset);

    }

    // Update is called once per frame
    void Update()
    {
        HandleJump();
        HandleAnimation();
        

        if (dedAnim == true)
        {
            StartCoroutine(DedDelay());
        }
        rb2D.gravityScale = tapHandler.gravityScalee;


        if(rb2D.gravityScale > 0.1f)
        {
            UpdateScore();
        }
    }

    void FixedUpdate()
    {
        if(dedAnim == true)
        {
            rb2D.velocity = Random.insideUnitCircle.normalized * 2f * jumpModifier;
            transform.Rotate(0, 0, dedRotation * Time.fixedDeltaTime);
        }
        
    }

    public void OnCollisionEnter2D(Collision2D coll)
    {
        GameObject obj = coll.collider.gameObject;

        if(obj.tag == "dokanmaOldurur")
        {

            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            dedAnim = true;
            rb2D.gravityScale = 0;
            
            SfxManager.PlayByNumber(0);            
            if (obj.GetComponent<Rigidbody2D>())
            {
                Destroy(obj.GetComponent<Rigidbody2D>());
            }
        } 
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        GameObject obj = coll.gameObject;

        if(obj.tag == "odul"){
            seconds += ballPoints;
            //score.text = (int.Parse(score.text) + 50).ToString();
            //scoreReload.text = (int.Parse(score.text) + 50).ToString();
        }
    }

    private void HandleJump()
    {
        if (attemptJump && timeUntilJump <= 0)
        {

            //int randomIndexSynth = Random.Range(0, 1500);
            //synthAManager.PlayByNumber(randomIndexSynth);

            SfxManager.PlayByNumber(3);

            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpModifier);
            timeUntilJump = jumpCD;
            attemptJump = false;
        }
        else
        {
            timeUntilJump -= Time.deltaTime;
        }
        
        if(rb2D.velocity.y >= 0.1f)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 20f);
        }
        else 
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 5f * rb2D.velocity.y);
        }
        
    }

    public void UpdateScore()
    {
        seconds += Time.deltaTime * 10;

        score.text = ((int)seconds).ToString();
        scoreReload.text = ((int)seconds).ToString();

    }

    private void HandleAnimation()
    {
        animator.SetFloat("vertical", rb2D.velocity.y);
        animator.SetBool("dedAnim", dedAnim);
    }

    private void SelfDestruct()
    {
        if(PlayerPrefs.GetInt("HighScore", 0) < seconds)
        {
            PlayerPrefs.SetInt("HighScore", (int) seconds);
        }

        gameObject.SetActive(false);
        Destroy(gameObject);
        youDead = true;
    }

    IEnumerator DedDelay()
    {
        yield return new WaitForSeconds(DedDelay_);
        SelfDestruct();
    }

}
