using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class penisPotuMovement : MonoBehaviour
{
    float timer = 10f;
    float speed = 1f;

    float speedOffset = 0.2f;
    float rotOffset = 90f;
    float rotation = -2f;
    // Start is called before the first frame update
    void Start()
    {
        
        Destroy(gameObject, timer);
        speed += Random.Range(-speedOffset, speedOffset);
        rotation += Random.Range(-rotOffset, rotOffset);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += new Vector3(-speed*Time.fixedDeltaTime, 0f ,0f);
        transform.Rotate(0,0,rotation * Time.fixedDeltaTime);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        GameObject obj = coll.gameObject;

        if(obj.tag == "Player")
        {
            SelfDestruct();
        }
    }

    private void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
