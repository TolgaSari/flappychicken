﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using System;
using UnityEngine;

public class SynthAudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public Sound drums;
    private float drumsLength;
    public static SynthAudioManager instance;
    int lastNumberPlayed;
    float slotTime;

    bool slot, playing;
    int scheduledSong;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        //foreach (Sound s in sounds)
        //{
        //    s.source = gameObject.AddComponent<AudioSource>();
        //    s.source.clip = s.clip;
        //    s.source.volume = 0.5f;
        //    s.source.pitch = 1f;
        //    s.source.loop = s.loop;
        //    s.source.playOnAwake = false;
        //}

        drums.source = gameObject.AddComponent<AudioSource>();
        drums.source.clip = drums.clip;
        drums.source.volume = 0.25f;
        drums.source.pitch = 1f;
        drums.source.loop = drums.loop;
        drums.source.loop = true;

    }

    void Start()
    {
        //Play("Theme");
        slotTime = 0;
         drums.source.Play();
    }
    
    private void FixedUpdate()
    {
        //slotTime += Time.deltaTime;
        //if(slotTime > 1f)
        //{
        //   slotTime = 0f;
       
        //   drums.source.Play();
        //   if(slot)
        //   {
        //       sounds[scheduledSong].source.Play();
        //       slot = false;
        //   }
        //}
        //else if(isInterval(slotTime, 0.25f) && slot)
        //{
        //    slot = false;
        //    sounds[scheduledSong].source.Play();
        //}
        //else if(isInterval(slotTime, 0.5f) && slot)
        //{
        //    slot = false;
        //    sounds[scheduledSong].source.Play();
        //}
        //else if(isInterval(slotTime, 0.75f) && slot)
        //{
        //    slot = false;
        //    sounds[scheduledSong].source.Play();
        //}
    }
    private bool isInterval(float value, float interval)
    {
        float delta = 0.1f;
        if((value > interval - delta) && (value < interval + delta))
        {
            return true;
        }
        return false;
    }
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound" + name + " not found!");
            return;
        }

        s.source.Play();
    }

    public void PlayByNumber(int number)
    {

        slot = true;

        scheduledSong = number % sounds.Length;
        /* 
        if (number < sounds.Length)
        {
            sounds[lastNumberPlayed].source.Stop();
            sounds[number].source.Play();
            lastNumberPlayed = number;
        }
        else
        {
            Debug.LogError("Synth Audio Manager: Sound index out of bounds!");
            return;
        }
        */
    }
}
