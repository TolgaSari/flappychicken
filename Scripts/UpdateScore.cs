using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScore : MonoBehaviour
{
    // Start is called before the first frame update
    public Text score;
    //public Text ball;
    void Start()
    {
        //ball.text = PlayerPrefs.GetInt("BallCount", 0).ToString();
        score.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //ball.text = PlayerPrefs.GetInt("BallCount", 0).ToString();
        score.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }
}
