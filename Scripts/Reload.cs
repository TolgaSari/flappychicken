﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reload : MonoBehaviour
{
    public MovementHandler DeadControl;
    public GameObject EndGameUI;
    public GameObject scoreCanvas;
    
    void Start()
    {
        scoreCanvas = GameObject.Find("ScoreCanvas");        
    }

    void Update()
    {
        if (DeadControl.youDead)
        {
            StartCoroutine(Delay());
        }
        else
        {
            scoreCanvas.SetActive(true);
            EndGameUI.SetActive(false);
            //Time.timeScale = 1f;

        }
    }
    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        scoreCanvas.SetActive(true);
        ScoreKeeping.scoreValue = 0;
        Time.timeScale = 1f;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.3f);
        scoreCanvas.SetActive(false);
        EndGameUI.SetActive(true);
        Time.timeScale = 0f;
    }
}
