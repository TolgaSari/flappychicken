﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    //public MovementHandler DeadControl;

    public GameObject synthAudioManager = null;
    private AudioSource synthAManagerAudio;

    public GameObject sfxManager = null;
    public SoundEffectsManager SfxManagerAudio;

    public GameObject PauseMenuUI;
    public GameObject PauseButtonUI;
    public GameObject SoundOffButton;
    public GameObject SoundOnButton;
    public GameObject MusicOnButton;
    public GameObject MusicOffButton;

    public bool soundOn = true;
    public bool musicOn= true;

    public void Start()
    {
        soundOn = intToBool(PlayerPrefs.GetInt("soundOn", boolToInt(true)));
        musicOn = intToBool(PlayerPrefs.GetInt("musicOn", boolToInt(true)));

        synthAudioManager = GameObject.Find("SynthAudioManager");
        synthAManagerAudio = synthAudioManager.GetComponent<AudioSource>();

        sfxManager = GameObject.Find("SoundEffectsManager");
        SfxManagerAudio = sfxManager.GetComponent<SoundEffectsManager>();

        if (musicOn == true)
        {

            MusicOnButton.SetActive(false);
            MusicOffButton.SetActive(true);
        }
        else
        {

            MusicOnButton.SetActive(true);
            MusicOffButton.SetActive(false);
        }

        if (SfxManagerAudio.soundOn == true)
        {
            //SfxManagerAudio.volume = 0.7f;
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
        }
        else
        {
            //SfxManagerAudio.volume = 0f;
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
        }
    }
    void Update()
    {

    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        PauseButtonUI.SetActive(true);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }
    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        PauseButtonUI.SetActive(false);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ButtonClick()
    {
        Pause();
    }

    public void Options()
    {

    }

    public void SoundOff()
    {
        SfxManagerAudio.soundOn = !SfxManagerAudio.soundOn;
        PlayerPrefs.SetInt("soundOn", boolToInt(SfxManagerAudio.soundOn));
        if (SfxManagerAudio.soundOn == true)
        {
            //SfxManagerAudio.volume = 0.7f;
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
        }
        else
        {
            //SfxManagerAudio.volume = 0f;
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
        }
    }


    public void MusicOff()
    {
        musicOn = !musicOn;
        PlayerPrefs.SetInt("musicOn", boolToInt(musicOn));
        if (musicOn==true)
        {
            synthAManagerAudio.volume = 0.25f;
            MusicOnButton.SetActive(false);
            MusicOffButton.SetActive(true);
        }
        else
        {
            synthAManagerAudio.volume = 0f;
            MusicOnButton.SetActive(true);
            MusicOffButton.SetActive(false);
        }
    }

    public int boolToInt(bool val)
    {
        if (val)
            return 1;
        else
            return 0;
    }

    public bool intToBool(int val)
    {
        if (val != 0)
            return true;
        else
            return false;
    }


}
